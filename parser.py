import argparse
import csv
import re
from collections import OrderedDict

source_path = 'testdata.txt'
output_path = 'result.csv'

# read optional command line arguments
argparser = argparse.ArgumentParser()
argparser.add_argument('-s', '--source', help='path to the source txt file')
argparser.add_argument('-o', '--output', help='path to the output csv file')
args = argparser.parse_args()
if args.source:
    source_path = args.source
if args.output:
    output_path = args.output

lines = []
record = OrderedDict()
records = []

# read source file
with open(source_path, 'r', encoding='windows-1252') as s:
    lines = s.readlines()

# remove first line as it would get picked up by the regex parser
lines.pop(0)

for line in lines:
    # add the last valid record to result and start a new record 
    if re.match('----', line):
        if len(record) > 0:
            records.append(record)
        record = OrderedDict()
    # keep adding valid entries to the current record
    else:
        entry = re.findall(r'^(.+)\: (.+)$', line)
        if len(entry) > 0:
            record.update(entry)

# add the last record because append in for loop isn't called anymore
records.append(record)

# write csv
with open(output_path, 'w', newline='') as csvfile:
    fieldnames = records[0].keys()
    writer = csv.DictWriter(csvfile, fieldnames=fieldnames, dialect='excel')

    writer.writeheader()
    writer.writerows(records)
